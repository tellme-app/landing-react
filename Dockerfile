FROM node:12-alpine as build
WORKDIR /app
COPY package.json /app/package.json
RUN npm install
COPY . /app
RUN npm run build
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
ENV REACT_APP_GOOGLE_FORM_URL https://docs.google.com/forms/d/e/1FAIpQLSfovjXMqd93xorWMlJx-LT1aJSyhH3FBke1d2lwZtLFlObe3A/viewform?usp\=sf_link
EXPOSE 80
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]
