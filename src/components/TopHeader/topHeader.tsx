import React, {FunctionComponent} from "react";
import s from "./TopHeader.module.scss"
import logo from './static/text_logo.png'
import {Link} from "react-router-dom";
import {LocalNameProps, LoginPageProps} from "../../App";
import ChooseLang from "./chooseLang";
import {ChangeLangProps} from "./chooseLang/chooseLang";

const TopHeader: FunctionComponent<LoginPageProps & LocalNameProps & ChangeLangProps> = ({address, data, userLang, setUserLang}) => {
    return (
        <div className={s.headerRow}>
            <div className={[s.leftBlock, s.block].join(' ')}>
                <Link to="/">
                    <img src={logo} alt="SpeakaTalka" className={s.logo}/>
                </Link>
                <p className={s.sloganText}>{data.Tagline}</p>
            </div>
            <div className={[s.rightBlock, s.block].join(' ')}>
                <Link
                    className={[s.button, s.login].join(' ')}
                    type="button"
                    to={address}
                >
                    {data.SignIn}
                </Link>
                <Link
                    className={[s.button, s.register].join(' ')}
                    type="button"
                    to={address}
                >
                    {data.SignUp}
                </Link>
                <ChooseLang classname={s.chooseLanguage} userLang={userLang} setUserLang={setUserLang}/>

            </div>
        </div>
    )
}

export default TopHeader;
