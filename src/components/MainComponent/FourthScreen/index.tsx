import React, {FC} from 'react';
import s from './FourthScreen.module.scss'
import bigAppScreen from './static/bigAppScreen-min.png'
import smallAppScreen from './static/smallAppScreen-min.png'
import TitleText from "../../SimpleComponents/TitleText";
import ParagraphText from "../../SimpleComponents/ParagraphText";
import {LocalNameProps} from "../../../App";

const FourthScreen: FC<LocalNameProps> = ({data}) => {
    return (
        <div className={s.container}>
            <div className={s.firstContainer}>
                <TitleText className={s.title}>
                    {data.FourthScreenFirstTitle}
                </TitleText>
                <TitleText className={s.title}>
                    {data.FourthScreenSecondTitle}
                </TitleText>
                <ParagraphText className={s.text}>
                    {data.FourthScreenFirstText}
                </ParagraphText>
            </div>
            <div className={s.secondContainer}>
                <div className={s.smallTextContainer}>
                    <TitleText className={s.title}>
                        {data.FourthScreenThirdTitle}
                    </TitleText>
                    <ParagraphText
                        className={s.text}>
                        {data.FourthScreenSecondText}
                    </ParagraphText>
                </div>
                <img src={bigAppScreen} className={s.bigImg} alt=""/>
                <img src={smallAppScreen} className={s.smallImg} alt=""/>
                <div className={s.bigTextContainer}>
                    <TitleText className={s.title}>
                        {data.FourthScreenThirdTitle}
                    </TitleText>
                    <ParagraphText
                        className={s.text}>
                        {data.FourthScreenSecondText}
                    </ParagraphText>
                </div>
            </div>
        </div>
    );
}

export default FourthScreen;
