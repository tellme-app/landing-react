import React, {FunctionComponent} from "react";
import s from './SecondScreen.module.scss'
import TitleText from "../../SimpleComponents/TitleText";
import ParagraphText from "../../SimpleComponents/ParagraphText";
import ContentPane from "../../SimpleComponents/ContentPane";
import travel from './static/travel-min.png';
import havingFun from './static/havingFun-min.png';
import interview from './static/interview-min.png';
import {LocalNameProps} from "../../../App";


const SecondScreen: FunctionComponent<LocalNameProps> = ({data}) => {

    return (
        <div className={s.container}>
            <div className={s.wrapper}>
                <div className={s.textContainer}>
                    <TitleText
                        className={s.title}>
                        {data.SecondScreenFirstTitle}
                    </TitleText>
                    <ParagraphText className={s.firstText}>
                        {data.SecondScreenText}
                    </ParagraphText>
                </div>
            </div>
            <TitleText className={s.centredTitle}>
                {data.SecondScreenSecondTitle}
            </TitleText>
            <div className={s.contentPaneContainer}>
                <div className={s.box}>
                    <ContentPane
                        imagePath={travel}
                        text={data.ContentPaneTravel}
                        bigImage
                    />
                </div>
                <div className={s.box}>
                    <ContentPane
                        imagePath={interview}
                        text={data.ContentPaneInterview}
                        bigImage
                    />
                </div>
                <div className={s.box}>
                    <ContentPane
                        imagePath={havingFun}
                        text={data.ContentPaneChatting}
                        bigImage
                    />
                </div>
            </div>
        </div>
    );
}

export default SecondScreen;
