import React, {FunctionComponent} from "react";
import s from "./ThirdScreen.module.scss";
import TitleText from "../../SimpleComponents/TitleText";
import ContentPane from "../../SimpleComponents/ContentPane";
import ellipse from "./static/Ellipse-min.png";
import polygon from "./static/Polygon-min.png";
import rectangle from "./static/Rectangle-min.png";
import {Link} from "react-router-dom";
import {LocalNameProps, LoginPageProps} from "../../../App";

const ThirdScreen: FunctionComponent<LoginPageProps & LocalNameProps> = ({address, data}) => {
    return (
        <div className={s.container}>
            <Link
                className={s.button}
                type="button"
                to={address}
            >
                {data.StartCommunicateButton}
            </Link>
            <TitleText className={s.title}>
                {data.ThirdScreenTitle}
            </TitleText>
            <div className={s.contentPaneContainer}>
                <div className={s.box}>
                    <ContentPane
                        subTitle={data.ContentPaneEnterTitle}
                        imagePath={ellipse}
                        text={data.ContentPaneEnterText}
                    />
                </div>
                <div className={s.box}>
                    <ContentPane
                        subTitle={data.ContentPaneProcessingTitle}
                        imagePath={polygon}
                        text={data.ContentPaneProcessingText}
                    />
                </div>
                <div className={s.box}>
                    <ContentPane
                        subTitle={data.ContentPaneExitTitle}
                        imagePath={rectangle}
                        text={data.ContentPaneExitText}
                    />
                </div>
            </div>
        </div>
    );
}

export default ThirdScreen;
