import React, {FunctionComponent} from 'react'
import s from './FifthScreen.module.scss'
import TitleText from "../../SimpleComponents/TitleText";
import {Link} from 'react-router-dom';
import {LocalNameProps, LoginPageProps} from "../../../App";

const FifthScreen: FunctionComponent<LoginPageProps & LocalNameProps> = ({address, data}) => {
    return (
        <div className={s.container}>
            <TitleText className={s.text}>
                {data.FifthScreenFirstTitle}
            </TitleText>
            <TitleText className={[s.text, s.secondText].join(' ')}>
                {data.FifthScreenSecondTitle}
            </TitleText>
            <Link
                className={s.button}
                type="button"
                to={address}
            >
                {data.StartLearnFreeButton}
            </Link>
        </div>
    );
}

export default FifthScreen;
