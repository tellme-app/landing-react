import React, {FunctionComponent} from 'react';
import s from './FinalScreen.module.scss'
import TitleText from "../../SimpleComponents/TitleText";
import ParagraphText from "../../SimpleComponents/ParagraphText";
import AnalyticImg from './static/analytic-min.png'
import DoneImg from './static/done-min.png'
import {Link} from "react-router-dom";
import {LocalNameProps, LoginPageProps} from "../../../App";

const FinalScreen: FunctionComponent<LoginPageProps & LocalNameProps> = ({address, data}) => {
    return (
        <div className={s.container}>
            <TitleText className={s.fullAnalyticsText}>
                {data.FinalScreenFirstTitle}
            </TitleText>
            <div className={s.analyticContainer}>
                <div className={s.textContainer}>
                    <TitleText className={s.title}>
                        {data.FinalScreenSecondTitleFirstPlain + " "}<span
                        className={s.whiteText}>{data.FinalScreenSecondTitleFirstAccident + " "}</span>{data.FinalScreenSecondTitleSecondPlain + " "}<span
                        className={s.whiteText}>{data.FinalScreenSecondTitleSecondAccident}</span>
                    </TitleText>
                    <ParagraphText className={s.text}>
                        {data.FinalScreenText}
                    </ParagraphText>
                    <Link
                        className={s.testLessonButton}
                        type="button"
                        to={address}
                    >
                        {data.TestLessonButton}
                    </Link>
                </div>
            </div>
            <img className={s.analyticImg} src={AnalyticImg} alt="Аналитика"/>
            <Link to={address} className={s.startLearnButton}>
                <p>{data.LearnToSpeakEnglishButton}</p>
            </Link>
            <Link to={address}>
                <img className={s.doneImg} src={DoneImg} alt="Выполнено"/>
            </Link>
        </div>
    )
}

export default FinalScreen;
