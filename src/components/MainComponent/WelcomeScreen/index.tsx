import React, {FC} from 'react'
import s from './WelcomeScreen.module.scss'
import TitleText from "../../SimpleComponents/TitleText";
import ParagraphText from "../../SimpleComponents/ParagraphText";
import {LocalNameProps} from "../../../App";


const WelcomeHeader: FC<LocalNameProps> = ({data}) => {

    return (
        <div className={s.container}>
            <div className={s.wrapper}>
                <div className={s.textContainer}>
                    <TitleText className={s.titleText}>
                        {data.WelcomeScreenTitle}
                    </TitleText>
                    <ParagraphText className={s.paragraph}>{data.WelcomeScreenFirstText}</ParagraphText>
                    <ParagraphText >{data.WelcomeScreenSecondText}</ParagraphText>
                </div>
            </div>
        </div>
    );

}

export default WelcomeHeader;
