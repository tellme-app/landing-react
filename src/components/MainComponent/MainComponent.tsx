import React, {FunctionComponent} from "react";
import WelcomeHeader from "./WelcomeScreen";
import SecondScreen from "./SecondScreen";
import ThirdScreen from "./ThirdScreen";
import FourthScreen from "./FourthScreen";
import FifthScreen from "./FifthScreen";
import FinalScreen from "./FinalScreen";
import {LocalNameProps, LoginPageProps} from "../../App";

const MainComponent: FunctionComponent<LoginPageProps & LocalNameProps> = ({address, data}) => {


    return (
        <>
            <WelcomeHeader data={data}/>
            <SecondScreen data={data}/>
            <ThirdScreen address={address} data={data}/>
            <FourthScreen data={data}/>
            <FifthScreen address={address} data={data}/>
            <FinalScreen address={address} data={data}/>
        </>
    )
}

export default MainComponent;
