import React, {ChangeEvent, FormEvent, FunctionComponent, useState} from 'react'
import s from './ModalWindow.module.scss'
import TitleText from "../SimpleComponents/TitleText";
import {Link} from "react-router-dom";
import {LocalNameProps} from "../../App";

interface HubspotError {
    errorType: string
}

interface HubspotErrorArray {
    errors: HubspotError[];
}

const ModalWindow: FunctionComponent<LocalNameProps> = ({data}) => {

    const [nameValue, setNameValue] = useState<string>("")
    const [emailValue, setEmailValue] = useState<string>("")
    const [socialMediaValue, setSocialMediaValue] = useState<string>("");
    const [emailError, setEmailError] = useState<boolean>(false);
    const [nameError, setNameError] = useState<boolean>(false);
    const [agreeValue, setAgreeValue] = useState<boolean>(false);
    const [agreeError, setAgreeError] = useState<boolean>(false);

    const onFormSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let error = false;
        setNameError(false);
        setEmailError(false);
        setAgreeError(false);
        if (!emailValue.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
            error = true;
            setEmailError(true);
        }
        if (!nameValue) {
            error = true;
            setNameError(true);
        }
        if (!agreeValue) {
            setAgreeError(true);
            error = true;
        }

        if (error)
            return;

        const reqBody = JSON.stringify({
            name: nameValue,
            email: emailValue,
            socialMedia: socialMediaValue ? socialMediaValue : ""
        })

        fetch(`/hubspot/contact`, {
            method: 'POST',
            body: reqBody,
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {
            if (response.status === 400) {
                if (response.body)
                    response.json()
                        .then((data: HubspotErrorArray) => {
                            if (data.errors)
                                // eslint-disable-next-line array-callback-return
                                data.errors.map((error: HubspotError) => {
                                    if (error.errorType.includes("EMAIL"))
                                        setEmailError(true);
                                })
                        })
            } else if (!response.ok) {
                console.log("Error! " + response.statusText)
            }
        })
    }

    const onEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
        setEmailValue(e.target.value);
    }

    const onNameChange = (e: ChangeEvent<HTMLInputElement>) => {
        setNameValue(e.target.value);
    }

    const onSocialMediaChange = (e: ChangeEvent<HTMLInputElement>) => {
        setSocialMediaValue(e.target.value);
    }

    const onAgreeChange = (e: ChangeEvent<HTMLInputElement>) => {
        setAgreeValue(e.target.checked);
    }

    const emailClasses = emailError ? [s.email, s.error].join(' ') : s.email;
    const nameClasses = nameError ? [s.name, s.error].join(' ') : s.name;
    const agreeClasses = agreeError ? [s.agree, s.error].join(' ') : s.agree;

    return (
        <div className={s.wrapper}>
            <div className={s.window}>
                <div className={s.leftPart}>
                    <TitleText className={s.title}>
                        {data.ModalTagline}
                    </TitleText>
                </div>
                <div className={s.rightPart}>
                    <form onSubmit={onFormSubmit}>
                        <div className={s.formTopContainer}>
                            <div className={s.inputContainer}>
                                <div className={nameClasses}>
                                    <input type="text" onChange={onNameChange} placeholder="Name"
                                           value={nameValue}/>
                                </div>
                                {nameError ?
                                    <span style={{top: '-30px'}} className={s.errorMessage}>{data.ModalNameError}</span> : ''}
                            </div>
                            <div className={s.inputContainer}>
                                <div className={emailClasses}>
                                    <input type="email" onChange={onEmailChange} placeholder="Email"
                                           value={emailValue}/>
                                </div>
                                {emailError ? <span style={{top: '-25px'}} className={s.errorMessage}>{data.ModalEmailError}</span> : ''}
                            </div>
                            <div className={s.inputContainer}>
                                <div className={s.socialMedia}>
                                    <input type="text" onChange={onSocialMediaChange} placeholder="Social Media"
                                           value={socialMediaValue}/>
                                </div>
                            </div>
                        </div>
                        <input type="submit" className={s.doneButton} value={data.ModalDone}/>
                        <label className={agreeClasses}>
                            <input type="checkbox" checked={agreeValue} onChange={onAgreeChange}/>
                            <Link to="/privacy" target="_blank">{data.ModalAgreementLink}</Link><br/>{data.ModalAgreement}
                        </label>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ModalWindow;
