import React from "react";
import s from './ParagraphText.module.scss';

interface SimpleTextProps extends React.HTMLAttributes<HTMLParagraphElement>{
    className?: string
}

const ParagraphText = ({className, children, ...other}: SimpleTextProps) => {
    const classes = [s.paragraphText];
    if (className){
        classes.push(className);
    }
    return(
        <p className={classes.join(' ')} {...other}>{children}</p>
    )
}

export default ParagraphText;
