import React from "react";
import s from "./TitleText.module.scss";
interface SimpleTextProps extends React.HTMLAttributes<HTMLDivElement> {
    className?: string
}

const TitleText = ({className, children, ...other} : SimpleTextProps) => {
    return(
        <p className={s.titleText + ' ' + className} {...other}>{children}</p>
    )
}

export default TitleText;
